const float inf= std::numeric_limits<float>::infinity();


struct Plan
{
    Point a;        // point sur le plan
    Vector n;       // normale du plan
    Color color;    // couleur
};

struct Sphere
{
    Point c;        // centre
    float r;       // rayon
    Color color;    // couleur
};

struct Triangle
{
    Point a;
    Point b;
    Point c;
    Color color;
};

struct Rectangle 
{
    Rectangle(float minX, float maxX, float minY, float maxY, float z, Color col){
        a = Point(minX, minY, z);
        b = Point(maxX, minY, z);
        c = Point(maxX, maxY, z);
        d = Point(minX, maxY, z);
        color = col;
    };

    Point a;
    Point b;
    Point c;
    Point d;
    Color color;
};

struct Hit
{
    float t;        // position sur le rayon ou inf
    Vector n;       // normale du point d'intersection, s'il existe
    Color color;    // couleur du point d'intersection, s'il existe

    operator bool() { return t >= 0 && t < inf; } // renvoie vrai si l'intersection existe et quelle est valide...
};


struct Lumiere {
    Point source;
    Color emission;
};






struct Panneau {
    Rectangle rectangle;
    int nb_source;
    void ajout(std::vector<Lumiere>& lumieres){
        Vector hor = Vector(rectangle.a, rectangle.b);
        Vector ver = Vector(rectangle.a, rectangle.d);


        for (int i = 0; i < std::sqrt(nb_source); i++){
            for (int j = 0; j < std::sqrt(nb_source); j++){

                
                Point p = rectangle.a +  j * hor  *  i * ver;
                lumieres.push_back( {p, rectangle.color} );
            }
        }

    };
};


struct Scene
{
    std::vector<Sphere> spheres;
    std::vector<Rectangle> rectangles;

    Plan plan;
};


Hit intersectPlan( const Plan& plan, const Point& o, const Vector& d )
{
    float t = dot(plan.n, Vector(o, plan.a)) / dot(plan.n, d);

    if(t < 0)
        return { inf, plan.n, plan.color };  // l'intersection n'est pas valide / derriere l'origine du rayon
    else
        return { t, plan.n, plan.color };    // renvoie la position de l'intersection, + la normale et la couleur du plan
}



Hit intersectSphere( const Sphere& sphere, const Point& o, const Vector& d )
{
    float a = dot(d,d);
    float b = 2*dot(d, Vector(sphere.c, o));
    float k = dot(Vector(sphere.c,o), Vector(sphere.c, o)) - (sphere.r*sphere.r);

    float det= b*b - 4*a*k;

    float t = inf;

    if (det >= 0) {
        float t1 = (-b + std::sqrt(det)) / (2*a);
        float t2 = (-b - std::sqrt(det)) / (2*a);

        
        if(t1 > 0 && t1 < t)
            t= t1;
        if(t2 > 0 && t2 < t)
            t= t2;
 
    } 

    if(t <  inf) {
        Point p = o + t*d; // point d'intersection
        return { t, normalize(Vector (sphere.c, p)), sphere.color };    // renvoie la position de l'intersection, + la normale et la couleur du plan

    }
  return { inf, Vector(), sphere.color };  // l'intersection n'est pas valide / derriere l'origine du rayon
 }



Hit intersectTriangle (const Triangle& tri,  const Point& o, const Vector& d ){
   
    // Normale
    Vector n = cross ( Vector (tri.a , tri.b ) , Vector (tri.a , tri.c ) ) ;

    // Intersection avec le rayon o , d
    float t = dot (n , Vector (o , tri.a)) / dot (n , d) ;

    // point d'intersection
    Point p = o + t * d ;

    // l'intersection n'est pas valide / derriere l'origine du rayon
    if ( ( dot (n , cross ( Vector (tri.a , tri.b ) , Vector (tri.a , p ) ) ) < 0) 
        || ( dot (n , cross ( Vector (tri.b , tri.c ) , Vector (tri.b , p ) ) ) < 0)
        || ( dot (n , cross ( Vector (tri.c , tri.a ) , Vector (tri.c , p ) ) ) < 0)
    ) {
        return { inf, Vector(), tri.color };  
    }
    
    return { t, normalize(n), tri.color };
}




Hit intersectRectangle(const Rectangle& rec, const Point& o, const Vector& d){

    Triangle haut = {rec.a, rec.b, rec.c, rec.color};
    Triangle bas = {rec.a, rec.c, rec.d, rec.color};

    Hit t1 = intersectTriangle(haut, o, d);
    Hit t2 = intersectTriangle(bas, o, d);

    Hit t = { inf, Vector(), rec.color };

    if(t1.t > 0 && t1.t < t.t)
            t = t1;
    if(t2.t > 0 && t2.t < t.t)
        t = t2;
    return t;
}



Hit intersect( const Scene& scene, const Point& o, const Vector& d)
{
    Hit s ;//= intersectSphere(scene.spheres[0], o, d);
    s.t = inf;

    // Spheres
    for(unsigned i= 0 ; i < scene.spheres.size(); i++)
    {
        // tester la ieme sphere
        Hit tmp= intersectSphere(scene.spheres[i], o, d);
        if(tmp) {  

            if (tmp.t < s.t) {
                s = tmp;      
            }
        }
    }
    // Rectangles
    for(unsigned i= 0 ; i < scene.rectangles.size(); i++)
    {
        // tester le triangle du haut et du bas de chaque rectangle
       
        Hit  tmp = intersectRectangle(scene.rectangles[i], o, d);

        if(tmp) {  
            if (tmp.t < s.t) {
                s = tmp;      
            }
        }
    }


    Hit h = intersectPlan(scene.plan, o, d);
    if(h) {
        if (h.t < s.t){
            s = h;  
        } 
    } 
  

    return s;

}