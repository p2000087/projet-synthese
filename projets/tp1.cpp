#include "image.h"
#include "image_io.h"
#include "vec.h"
#include <limits>
#include <cmath>
#include <algorithm>
#include "struct.hpp"
#include <vector>



int main( )
{
    Image image(1024, 1024);  

    // Sources de lumière
    std::vector<Lumiere> lumieres;

    lumieres.push_back({Point (-3,2,-5), Color(1, 0, 0)}); 
    lumieres.push_back({Point (3,8,-5), Color(0, 0, 1)}); 
    lumieres.push_back({Point (2,5,0), Color(0, 1, 0)});


    // Panneau Lumineux
    Panneau panneau = { Rectangle(3, 4, 7, 8, 10, Color(0.2,0.2,0.2)), 4 };

    panneau.ajout(lumieres);

    

    // Plan
    Plan plan = {Point (0,-1,0), Vector(0,1, 0), Color(1,1,1)};

    // Spheres
    std::vector<Sphere> sp; 
    sp.push_back({Point(-1,0,-3), 1, Color(1,0,1)});
    sp.push_back({Point(3,0,-5), 1, Color(1,1,0)}); 
    sp.push_back({Point(-1,0,-3.5), 1, Color(1,0,0)});
    sp.push_back({Point(2,1,-6), 1, Color(1,0,0)});



    // Ajout d'un rectangle
    std::vector<Rectangle> rec;
    // rec.push_back({Rectangle(0, 1, 0, 1, -4, Color(1,1,1))});

    //Scene
    Scene scene;
    scene.plan = plan;
    scene.spheres = sp;
    scene.rectangles = rec;


    for(int py= 0; py < image.height(); py++)
    for(int px= 0; px < image.width(); px++)
    {        
        float x = float ( px ) / float ( image . width () ) * 2 -1;
        float y = float ( py ) / float ( image . height () ) * 2 -1;
        float z = -1;

        Point o = Point(0, 0, 0);   // origine
        Point e = Point(x,y,z);    // extremite
        Vector d = Vector(o, e);   // direction : extremite - origine


        Hit h = intersect(scene , o, d);
        
            
        if (h){
            
            Point p = o + h.t * d ;
            p = p + h.n * 0.001 ; // on décale d'un epsilon
            Color pixel = Color (0,0,0);

            for (Lumiere lum : lumieres){
                // calcul éclairage
                Vector l = Vector( p, lum.source ) ;
                float cos_theta = std::max( float(0), dot( normalize(h.n), normalize(l))) ;

                // Calcul l'intersection avec la lumiere et la scene
                Hit i = intersect(scene, p, l) ;


                if (!i || (i.t > 1)) {  // Vérifier ombre
                    pixel = pixel + (lum.emission * h.color * cos_theta );
                } 

            }
            

            // ajouter le pixel
            image(px, py)= Color(pixel, 1) ;

        }

    }

    write_image(image, "image.png");
    
    return 0;
}



